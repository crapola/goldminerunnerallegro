#pragma once
#include "State.h"

class State_GameRunning : public State
{
	unsigned char timer;
	unsigned char currentLevel,maxLevel;
public:
	State_GameRunning(StateOwner);
	~State_GameRunning(void);
	int Logic();
	void Render();
};
