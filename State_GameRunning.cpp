#include "State_GameRunning.h"
#include "State_GameMenu.h"
#include "Game.h"

State_GameRunning::State_GameRunning(StateOwner o):State(o)
{
	srand(time(0));
	timer=100;
	currentLevel=0;
	maxLevel=owner.gamePtr->GetMaxLevel();
	owner.gamePtr->CreatePlayer();
	owner.gamePtr->ChangeLevel(0);
}
State_GameRunning::~State_GameRunning(void)
{
	owner.gamePtr->NewHigh();
	owner.gamePtr->DeletePlayer();
}

int State_GameRunning::Logic()
{
	if (Renderer::chrono>=0)
		Renderer::chrono--;
	owner.gamePtr->UpdateEntities();
	if (key[KEY_ESC]||owner.gamePtr->LivesLeft()==0)
	{
		while (key[KEY_ESC]) {};
		State* ss;
		ss=new State_GameMenu(owner);
		owner.gamePtr->ChangeState(ss);
		return 0;
	}
	if (owner.gamePtr->GetTilesLeft()==0)
	{
		if (Renderer::chrono>0)
		{
			//time bonus
			while (Renderer::chrono>0)
			{
				Renderer::BlitMap();
				owner.gamePtr->RenderEntities();
				Renderer::chrono-=100;
				owner.gamePtr->player->score++;
				owner.gamePtr->ShowScore();
				Renderer::DrawChrono();
				Renderer::Flip();
			}
		}
		currentLevel++;
		if (currentLevel>=maxLevel)
			currentLevel=0;
		owner.gamePtr->ChangeLevel(currentLevel);
	}
	if (owner.gamePtr->PlayerKilled())
	{
		Render();
		Renderer::Sound(11);
		rest(700);
		owner.gamePtr->ChangeLevel(currentLevel);
	}
	return 0;
}

void State_GameRunning::Render()
{
	Renderer::BlitMap();
	owner.gamePtr->RenderEntities();
	owner.gamePtr->ShowScore();
	Renderer::DrawChrono();
	Renderer::LevelCaption(currentLevel+1);
}
