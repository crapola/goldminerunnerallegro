#include "PlayerTestClass.h"

PlayerTestClass::PlayerTestClass(Map* theMap)
{
	x=0;
	y=0;
	vx=0;
	vy=0;
	moveTime=0;
	mapPtr=theMap;
	score=0;
	lives=3;
	bAlive=true;
	ticker=0;
	image=0;
}

PlayerTestClass::~PlayerTestClass(void)
{
}

void PlayerTestClass::Logic()
{
	mapPtr->playerx=x;
	mapPtr->playery=y;
	mapPtr->UpdateScentMap(x,y);
	if (!Move())
	{
		CheckGoldTile();
		if (Falling())
			return;
		if (key[KEY_UP])
		{
			image=4;
			InitMove(0,-1);
		}
		if (key[KEY_DOWN])
		{
			image=4;
			InitMove(0,1);
		}
		if (key[KEY_LEFT])
		{
			image=0;
			InitMove(-1,0);
		}
		if (key[KEY_RIGHT])
		{
			image=2;
			InitMove(1,0);
		}
		if (key[KEY_J])
		{
			Destroy(0);
			image=0;
		}
		if (key[KEY_K])
		{
			Destroy(1);
			image=2;
		}
	}
}

void PlayerTestClass::Render()
{
	Renderer::DrawSprite(x,y,image+ticker/4+1);
}

bool PlayerTestClass::Move()
{
	if (moveTime==0)
		return false;
	ticker++;
	ticker=ticker%8;
	x+=vx;
	y+=vy;
	moveTime--;
	return true;
}

void PlayerTestClass::InitMove(short xx,short yy)
{
	int t=mapPtr->GetTile((x+xx*16)/16,(y+yy*16)/16);
	if (t==1)
		return;
	if (yy==-1&&mapPtr->GetTile(x/16,y/16)!=2)
		yy=0;
	if (yy==1&&mapPtr->GetTile(x/16,y/16+1)!=2&&mapPtr->GetTile(x/16,y/16)!=2&&mapPtr->GetTile(x/16,y/16)!=3)
		yy=0;
	if (t==3)
	{
		image=4;
	}
	vx=xx;
	vy=yy;
	moveTime=16;
}

bool PlayerTestClass::Falling()
{
	if (y>=496)
	{
		y=-16;
		mapPtr->ResetScentMap();
	}
	int t=mapPtr->GetTile(x/16,y/16+1);
	bool onlad=mapPtr->GetTile(x/16,y/16)==2;
	onlad=onlad||(mapPtr->GetTile(x/16,y/16)==3);
	if (onlad||(t>0&&t<3))
		return false;
	y+=8;
	image=4;
	return true;
}

void PlayerTestClass::CheckGoldTile()
{
	int t=mapPtr->GetTile(x/16,y/16);
	if (t==4)
	{
		//on a gold bar
		mapPtr->ClearTile(x/16,y/16);
		mapPtr->tilesLeft--;
		score+=10;
		Renderer::Sound(14);
	}
	if (t==1)
	{
		//fucked
		bAlive=false;
	}
}

void PlayerTestClass::Destroy(int t)
{
	if (mapPtr->GetTile(x/16-1+t*2,y/16+1)!=1)
		return;
	Renderer::Sound(12);
	mapPtr->ClearTile(x/16-1+t*2,y/16+1);
	mapPtr->NewTimedTile(x/16-1+t*2,y/16+1);
}

void PlayerTestClass::Reset(int xx,int yy)
{
	x=xx;
	y=yy;
	moveTime=0;
	bAlive=true;
	image=0;
}
