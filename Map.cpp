#include "Map.h"

Map::Map(void)
{
	std::ifstream mapfile("maps.bin",std::ios::binary);
	usedMapsNum=0;
	if (mapfile.is_open())
	{
		char xt1,xt2;
		char packedtile;
		for (int i=0; i<256; i++)
		{
			bool wasEmptyMap=true;
			for (int x=0; x<16; x++)
			{
				for (int y=0; y<32; y++)
				{
					mapfile.read(&packedtile,1);
					xt1=(packedtile&0xF0)>>4;
					xt2=packedtile&0x0F;
					levels[i].Tiles[x*2][y]=xt1;
					levels[i].Tiles[x*2+1][y]=xt2;
					if (xt1==5||xt2==5)//found player start
					{
						wasEmptyMap=false;
					}
				}
			}
			if (wasEmptyMap)
			{
				usedMapsNum=i;
				break;
			}
		}
		if (usedMapsNum==0)
		{
			allegro_message("Invalid map file.");
			exit(1);
		}
		mapfile.close();
	}
	else
	{
		allegro_message("maps.bin not found!");
		exit(1);
	}

	tilesLeft=0;
	ClearTimedTiles();
	playerStartX=0;
	playerStartY=0;
	CreateSequence();
}

Map::~Map(void)
{
}

void Map::Render(int plvl,int lMode)//called each new level
{
	int lvl=plvl;
	if (lMode==1)
		lvl=sequence[plvl];
	tilesLeft=0;
	ClearTimedTiles();
	ClearGhostsSpawnPoints();
	ResetScentMap();
	Renderer::ClearMapBmp();
	for (int y=0; y<32; y++)
	{
		for (int x=0; x<32; x++)
		{
			Renderer::DrawTile(x,y,levels[lvl].Tiles[x][y]);
			currentMap.tile[x][y]=levels[lvl].Tiles[x][y];
			if (currentMap.tile[x][y]==5)//playerstart
			{
				playerStartX=x*16;
				playerStartY=y*16;
			}
			if (currentMap.tile[x][y]==4)//gold
			{
				tilesLeft++;
			}
			if (currentMap.tile[x][y]==6)//a ghost
			{
				GhostStart g;
				g.x=x*16;
				g.y=y*16;
				ghostStarts.push(g);
			}
		}
	}

}

int Map::GetTile(int x,int y)
{
	if (x<0||x>31||y<0||y>31)
		return 1;
	return currentMap.tile[x][y];
}

void Map::ClearTile(int x,int y)
{
	if (x<0||x>31||y<0||y>31)
		return;
	currentMap.tile[x][y]=0;
	Renderer::DrawTile(x,y,0);
}

void Map::NewTimedTile(int x,int y)
{
	if (x<0||x>31||y<0||y>31)
		return;
	TimedTile thisone;
	thisone.x=x;
	thisone.y=y;
	timedTiles.push(thisone);
}

void Map::UpdateTimedTiles()
{
	if (!timedTiles.empty())
	{
		timer--;
		if (!timer)
		{
			TimedTile caca=timedTiles.front();
			int sx=caca.x,sy=caca.y;
			currentMap.tile[sx][sy]=1;
			Renderer::DrawTile(caca.x,caca.y,1);
			Renderer::Sound(13);
			timedTiles.pop();
			timer=100;
		}
	}
}

void Map::ClearTimedTiles()
{
	while (!timedTiles.empty())
	{
		timedTiles.pop();
	}
	timer=100;
}

void Map::ClearGhostsSpawnPoints()
{
	while (!ghostStarts.empty())
	{
		ghostStarts.pop();
	}
}
void Map::ResetScentMap()
{
	for (int x=0; x<31; x++)
	{
		for (int y=0; y<31; y++)
		{
			currentMap.scentTile[x][y]=0;
		}
	}
}

void Map::UpdateScentMap(short px,short py)
{
	currentMap.scentTile[px/16][py/16]=60000;
	for (int x=0; x<31; x++)
	{
		for (int y=0; y<31; y++)
		{
			if (currentMap.scentTile[x][y]>0)
				currentMap.scentTile[x][y]--;
			//Renderer::TestScent(x,y,currentMap.scentTile[x][y]);
		}
	}
}

unsigned int Map::GetScentTile(short x,short y)
{
	if (x<0||x>31||y<0||y>31)
		return 0;
	if (currentMap.tile[x][y]==1)
		return 0;
	return currentMap.scentTile[x][y];
}

void Map::CreateSequence()
{
	for (int i=0; i<usedMapsNum; i++)
	{
		sequence[i]=i;
	}
	for (int i=0; i<usedMapsNum; i++)
	{
		int r=rand()%(usedMapsNum);
		unsigned char temp=sequence[i];
		sequence[i]=sequence[r];
		sequence[r]=temp;
	}

}
