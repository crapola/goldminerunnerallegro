#pragma once
#include "Renderer.h"
#include "Map.h"

enum DECISION
{
	IDLE=0,
	UP=1,
	DOWN,
	LEFT,
	RIGHT
};

class Ghost
{
	Map* mapPtr;
	short vx,vy;
	char moveTime;
	char tick;
	DECISION decision;
	bool bHasFallen;
	unsigned char imageticker;
public:
	unsigned char limboTime;
	bool bTouchedPlayer;
	short x,y;
	Ghost(Map*);
	~Ghost(void);
	void Logic();
	void Render();
	bool Move();
	void InitMove(short,short);
	bool Falling();
	void Reset(int,int);
	void WhatToDo();
	void CheckTile();
};
