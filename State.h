#pragma once
#include "Renderer.h"

class Game;

typedef union
{
	Game* gamePtr;
	long number;
} StateOwner;

class State
{
protected:
	StateOwner owner;
public:
	State(StateOwner o):owner(o) {};
	virtual ~State() {};
	virtual int Logic()=0;
	virtual void Render()=0;
};
