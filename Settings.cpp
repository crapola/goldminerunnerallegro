#include "Settings.h"
#include <allegro.h>

Settings::Settings(void)
{
	levelMode=0;
	std::ifstream file("user.bin",std::ifstream::out|std::ifstream::binary);
	if (!file.good())
	{
		Populate();
	}
	else
	{
		HighScore h;
		char* buff=new char[17];
		for (int i=0; i<10; i++)
		{
			file.read((char*)buff,17);
			h.name.assign(buff);
			file.read((char*)&h.score,8);
			entries.push_front(h);
		}
		file.close();
		delete[] buff;
	}

}

Settings::~Settings(void)
{
	std::ofstream file("user.bin",std::ofstream::in|std::ofstream::trunc|std::ofstream::binary);
	if (!file.good())
	{
		allegro_message("Error saving high scores");
	}
	HighScore h;
	for (int i=0; i<10; i++)
	{
		h=entries.back();
		file.write((char*)h.name.c_str(),17);
		file.write((char*)&h.score,8);
		entries.pop_back();
	}
	file.close();
}

void Settings::Populate()
{
	HighScore h;
	for (int i=0; i<10; i++)
	{
		h.score=(i+1)*250;
		h.name="Goldmine Runner";
		entries.push_back(h);
	}
}

HighScore& Settings::GetScore(int n)
{
	std::list<HighScore>::iterator it = entries.begin();
	std::advance(it, n);
	return (*it);
}

bool Settings::Compare(HighScore h1,HighScore h2)
{
	if (h1.score<h2.score)
		return true;
	return false;
}

void Settings::InsertScore(HighScore& hs)
{
	entries.push_back(hs);
	entries.sort(Settings::Compare);
	entries.pop_front();
}

int Settings::GetRank(HighScore& hs)
{
	if (hs.score<entries.front().score)
		return -1;
	std::list<HighScore>::iterator it=entries.end();
	int r=0;
	while (r<10)
	{
		it--;
		r++;
		if (hs.score>=(*it).score)
			return r;

	}
	return 10;
}
