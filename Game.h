#pragma once
#include "Renderer.h"
#include "Map.h"
#include "State_GameMenu.h"
#include "PlayerTestClass.h"
#include "Ghost.h"
#include "Settings.h"
#include <vector>
class Game
{
	Map map;
	static State* state;
	std::vector<Ghost*> ghosts;
public:
	Settings settings;
	PlayerTestClass* player;
	static bool bRunning;

	Game(void);
	~Game(void);
	void ChangeState(State*);
	void Update();
	void ChangeLevel(int);
	void UpdateEntities();
	void RenderEntities();
	void ShowScore();
	int GetTilesLeft();
	void CreatePlayer();
	void DeletePlayer();
	unsigned char GetMaxLevel();
	bool PlayerKilled();
	int LivesLeft();
	void DrawHighScores();
	void NewHigh();
};
