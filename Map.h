#pragma once
#include <iostream>
#include <fstream>
#include "Renderer.h"
#include <queue>

typedef struct
{
	char Tiles[32][32];
} Level;

typedef struct
{
	char tile[32][32];
	unsigned int scentTile[32][32];
} CurrentMap;

typedef struct
{
	char x,y;
} TimedTile;

typedef struct
{
	short x,y;
} GhostStart;

class Map
{
	char timer;
	Level levels[256];
	CurrentMap currentMap;
	std::queue<TimedTile> timedTiles;
	unsigned char sequence[256];
public:
	std::queue<GhostStart> ghostStarts;
	int tilesLeft;
	short playerStartX,playerStartY;
	unsigned char usedMapsNum;
	short playerx,playery;
	Map(void);
	~Map(void);
	void Render(int,int);
	int GetTile(int,int);
	void ClearTile(int,int);
	void NewTimedTile(int,int);
	void UpdateTimedTiles();
	void ClearTimedTiles();
	void ClearGhostsSpawnPoints();
	void ResetScentMap();
	void UpdateScentMap(short,short);
	unsigned int GetScentTile(short,short);
	void CreateSequence();
};
