#pragma once
#include <fstream>
#include <list>
#include <string>
#include <iostream>
#ifndef _MSC_VER
#include <inttypes.h>
#endif

struct HighScore
{
#ifdef _MSC_VER
	unsigned __int64 score;
#else
	uint64_t score;
#endif
	std::string name;
};

class Settings
{
	std::list<HighScore> entries;
public:
	unsigned char levelMode;
	Settings();
	~Settings();
	void Populate();
	HighScore& GetScore(int);
	void InsertScore(HighScore&);
	static bool Compare(HighScore,HighScore);
	int GetRank(HighScore&);
};
