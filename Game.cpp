#include "Game.h"

bool Game::bRunning;
State* Game::state;

Game::Game(void):map()
{
	//map.Render(0);
	StateOwner oo;
	oo.gamePtr=this;
	state=new State_GameMenu(oo);
	player=0;
	bRunning=true;
}

Game::~Game(void)
{
	if (state)
		delete state;
	if (player)
		delete player;
}

void Game::Update()
{
#ifdef _DEBUG
	if (!state)
	{
		allegro_message("?");
		bRunning=false;
	}
#endif
	if (Renderer::logicTicker)
	{
		state->Logic();
		Renderer::logicTicker=0;
		state->Render();
		Renderer::Flip();
	}
	else
	{
		rest(1);
	}

}

void Game::ChangeState(State* s)
{
	delete state;
	state=s;
}

void Game::ChangeLevel(int l)
{
	Renderer::chrono=2*6000;
	//delete ghosts
	for (size_t i=0; i<ghosts.size(); i++)
	{
		delete ghosts[i];
	}
	ghosts.clear();
	//render map
	map.Render(l,settings.levelMode);
	//reset player
	player->Reset(map.playerStartX,map.playerStartY);
	//create ghosts
	while (!map.ghostStarts.empty())
	{
		ghosts.push_back(new Ghost(&map));
		ghosts.back()->Reset(map.ghostStarts.front().x,
							 map.ghostStarts.front().y);
		map.ghostStarts.pop();
	}
}

void Game::UpdateEntities()
{
	if (!player)
		return;

	player->Logic();
	int combo=15;
	for (size_t i=0; i<ghosts.size(); i++)
	{
		ghosts[i]->Logic();
		if (ghosts[i]->bTouchedPlayer)
		{
			player->bAlive=false;
		}
		if (ghosts[i]->limboTime==255)
		{
			player->score+=combo;
			combo+=5;
		}
	}
	map.UpdateTimedTiles();
}

void Game::RenderEntities()
{
	if (!player)
		return;
	player->Render();
	for (size_t i=0; i<ghosts.size(); i++)
	{
		ghosts[i]->Render();
	}
}

void Game::ShowScore()
{
	Renderer::DrawScore(80,513,player->score);
	for (int x=0; x<player->lives; x++)
	{
		Renderer::DrawSprite(512-x*16,512,1);
	}
}

int Game::GetTilesLeft()
{
	return map.tilesLeft;
}
void Game::CreatePlayer()
{
	if (player!=0)
	{
		allegro_message("Error creating player");
		exit(1);
	}
	player=new PlayerTestClass(&map);
}
void Game::DeletePlayer()
{
	delete player;
	player=0;
	for (size_t i=0; i<ghosts.size(); i++)
	{
		delete ghosts[i];
	}
	ghosts.clear();
}

unsigned char Game::GetMaxLevel()
{
	map.CreateSequence();
	return map.usedMapsNum;
}

bool Game::PlayerKilled()
{
	if (player->bAlive)
		return false;
	player->lives--;
	return true;
}

int Game::LivesLeft()
{
	return player->lives;
}

void Game::DrawHighScores()
{
	Renderer::Rectangle(128,104,248,176,0);
	HighScore hs;
	for (int i=0; i<10; i++)
	{
		hs=settings.GetScore(i);
		Renderer::DrawScore(360,256-i*16,hs.score);
		Renderer::Text(hs.name,275,256-i*16,-1,1);
	}
}

void Game::NewHigh()
{
	HighScore hs;
	hs.name="";
	hs.score=player->score;
	int rank=settings.GetRank(hs);
	if (rank>-1)
	{
		std::ostringstream rankstr;
		std::string rs("You are number ");
		rankstr<<rs<<rank<<" .";
		if (rank==1)
			rankstr<<" Woohoo !";
		char ch=0;
		clear_keybuf();
		while (ch!=13)
		{
			Renderer::Text(rankstr.str(),64,214,-1,0);
			Renderer::Text("Enter your name :",64,230,-1,0);
			Renderer::Text(hs.name,64,256,-1,0);
			Renderer::Text("\x0DB",64+8*hs.name.size(),256,0xFF5500,0);
			Renderer::Flip();
			ch=(readkey()&0xff);
			if (ch==8)
			{
				hs.name=hs.name.substr(0,hs.name.size()-1);
				continue;
			}
			if (hs.name.size()<16&&ch>31&&ch<127)
			{
				hs.name+=ch;
			}

		}


		settings.InsertScore(hs);
	}
}
