#pragma once
#include <allegro.h>
#include <string>
#include <sstream>
class Renderer
{
private:
	static BITMAP* buffer;
	static BITMAP* mapBmp;
public:
	static DATAFILE* dataFile;
	static int logicTicker;
	static int chrono;
	static const int scrW,scrH;
	Renderer(void);
	~Renderer(void);
	static void Rectangle(int,int,int,int,int);
	static void Text(std::string string,int x,int y,int color,int mode);
	static void Flip();
	static void Timer();
	static void DrawTile(int,int,int);
	static void BlitMap();
	static void ClearMapBmp();
	static void TestScent(int,int,int);
	static void DrawSprite(int,int,int);
	static void DrawScore(int,int,unsigned long);
	static void Sound(int);
	static void DrawChrono();
	static void LevelCaption(int);
};
