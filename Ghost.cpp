#include "Ghost.h"

Ghost::Ghost(Map* theMap)
{
	x=0;
	y=0;
	vx=0;
	vy=0;
	moveTime=0;
	mapPtr=theMap;
	decision=(DECISION)(3+rand()%2);
	limboTime=0;
	tick=0;
	bTouchedPlayer=false;
	bHasFallen=true;//actually its true when the ghost
	//	is allowed to change direction !
	imageticker=0;
}

Ghost::~Ghost(void)
{
}

void Ghost::Logic()
{
	if (limboTime)
		return;

	if (((mapPtr->playerx+8)/16==(x+8)/16)&&((mapPtr->playery+8)/16==(y+8)/16))
	{
		bTouchedPlayer=true;
	}

	if (!Move())
	{
		WhatToDo();
		CheckTile();
		if (Falling())
		{
			bHasFallen=true;
			return;
		}
		switch (decision)
		{
		case UP:
			InitMove(0,-1);
			break;
		case DOWN:
			InitMove(0,1);
			break;
		case LEFT:
			InitMove(-1,0);
			break;
		case RIGHT:
			InitMove(1,0);
			break;
		case IDLE:
			break;
		}
	}
}

void Ghost::Render()
{
	if (limboTime)
	{
		limboTime--;
		return;
	}
	Renderer::DrawSprite(x,y,7+imageticker/10+(decision==RIGHT)*2);
}

bool Ghost::Move()
{
	if (moveTime==0)
	{
		return false;
	}
	x+=vx;
	y+=vy;
	moveTime--;
	imageticker++;
	imageticker=imageticker%20;
	return true;
}

void Ghost::InitMove(short xx,short yy)
{
	int t=mapPtr->GetTile((x+xx*16)/16,(y+yy*16)/16);
	if (t==1)//there's a wall
	{
		bHasFallen=true;
		return;
	}
	if (yy==-1&&mapPtr->GetTile(x/16,y/16)!=2)
	{
		yy=0;
		//bHasFallen=true;
	}
	if (yy==1&&mapPtr->GetTile(x/16,y/16+1)!=2&&mapPtr->GetTile(x/16,y/16)!=2&&mapPtr->GetTile(x/16,y/16)!=3)
	{
		yy=0;
		//bHasFallen=true;
	}
	if (xx==0&&yy==0)
		bHasFallen=true;
	vx=xx;
	vy=yy;
	moveTime=16;
}

bool Ghost::Falling()
{
	if (y>=496)
		y=-16;
	int t=mapPtr->GetTile(x/16,y/16+1);
	bool onlad=mapPtr->GetTile(x/16,y/16)==2;
	onlad=onlad||(mapPtr->GetTile(x/16,y/16)==3);
	if (onlad||(t>0&&t<3))
	{
		return false;
	}
	y+=8;

	return true;
}

void Ghost::Reset(int xx,int yy)
{
	x=xx;
	y=yy;
	moveTime=0;
	bHasFallen=true;
}

void Ghost::WhatToDo()
{
	if ((mapPtr->GetTile(x/16,y/16)==2)||(mapPtr->GetTile(x/16,y/16)==3)||(mapPtr->GetTile(x/16,y/16+1)==2))
	{
		bHasFallen=true;
	}
	if (!bHasFallen)
		return;
	bHasFallen=false;
	//first use scent
	unsigned int st=mapPtr->GetScentTile(x/16,y/16);
	if (st>0)
	{
		unsigned int st1=mapPtr->GetScentTile(x/16,y/16-1);
		unsigned int st2=mapPtr->GetScentTile(x/16,y/16+1);
		unsigned int st3=mapPtr->GetScentTile(x/16-1,y/16);
		unsigned int st4=mapPtr->GetScentTile(x/16+1,y/16);
		unsigned int max=st;
		if (st1>max)
		{
			if (mapPtr->GetTile(x/16,y/16)==2)
			{
				max=st1;
				decision=UP;
			}
		}
		if (st2>max)
		{
			if (mapPtr->GetTile(x/16,y/16+1)!=1)
			{
				max=st2;
				decision=DOWN;
			}
		}
		if (st3>max)
		{
			if (mapPtr->GetTile(x/16-1,y/16)!=1)
			{
				max=st3;
				decision=LEFT;
			}
		}
		if (st4>max)
			if (mapPtr->GetTile(x/16+1,y/16)!=1)
			{
				max=st3;
				decision=RIGHT;
			}
		if (max!=st)
			return;
	}
	//else basic
	if (rand()%1>0)
		return;
	char t=mapPtr->GetTile(x/16,y/16);
	//char tup=mapPtr->GetTile(x/16,y/16-1);
	char tdo=mapPtr->GetTile(x/16,y/16+1);
	if (mapPtr->playerx<x)
		decision=LEFT;
	if (mapPtr->playerx>x)
		decision=RIGHT;
	if (mapPtr->playery<y)
	{
		if (t==2)
			decision=UP;
	}
	if (mapPtr->playery>y)
	{
		if ((t==3||t==2||tdo==2)&&tdo!=1)
			decision=DOWN;
	}
}

void Ghost::CheckTile()
{
	int t=mapPtr->GetTile(x/16,(y+8)/16);
	if (t==1)
	{
		Renderer::Sound(15);
		Reset(16*(rand()%31),0);
		limboTime=255;
	}
}
