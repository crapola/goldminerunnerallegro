#include "Renderer.h"

BITMAP* Renderer::buffer;
BITMAP* Renderer::mapBmp;
DATAFILE* Renderer::dataFile;
const int Renderer::scrW=512;
const int Renderer::scrH=528;
int Renderer::logicTicker;
int Renderer::chrono;
Renderer::Renderer(void)
{
	allegro_init();
	set_uformat(U_ASCII);
	install_keyboard();
	install_mouse();
	reserve_voices(4,0);
	install_sound(DIGI_AUTODETECT,MIDI_AUTODETECT,NULL);
	logicTicker=0;
	install_int(&Renderer::Timer,10);//33=30fps
	set_color_depth(32);	// 32 is better for Vista/7
	show_os_cursor(MOUSE_CURSOR_ARROW);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED,scrW,scrH,0,0);
	buffer=create_bitmap(scrW,scrH);
	mapBmp=create_bitmap(512,512);

	//load datas.
	dataFile=NULL;
	packfile_password("caca");
	dataFile=load_datafile("assets.dat");
	if (!dataFile)
	{
		allegro_message("assets.dat not found.");
		exit(0);
	}
	font=(FONT*)dataFile[16].dat;
	set_palette(*((PALETTE*)dataFile[17].dat));
	//ufont=extract_font_range(font,0x80,0xFF);
	set_window_title("Goldmine Runner");
}

Renderer::~Renderer(void)
{
	destroy_bitmap(buffer);
	destroy_bitmap(mapBmp);
	unload_datafile(dataFile);
	remove_sound();
}

void Renderer::Rectangle(int x,int y,int w,int h,int color)
{
	rectfill(buffer,x,y,w+x,h+y,color);
	rect(buffer,x,y,w+x,h+y,0xFFFFFF);
}

void Renderer::Text(std::string s,int x,int y,int color,int mode)
{
	switch (mode)
	{
	case 0:
		textout_ex(buffer,font,s.c_str(),x,y,color,-1);
		break;
	case 1:
		textout_right_ex(buffer,font,s.c_str(),x,y,color,-1);
		break;
	case 2:
		textout_ex(buffer,font,s.c_str(),256-s.size()*4,y,color,-1);
		break;
	}

}

void Renderer::Flip()
{
	blit(buffer,screen,0,0,0,0,scrW,scrH);
	clear_bitmap(buffer);
}

void Renderer::Timer()
{
	logicTicker++;
}

void Renderer::DrawTile(int x,int y,int t)
{
	if (t==0)
		rectfill(mapBmp,x*16,y*16,x*16+15,y*16+15,0);
	if (t>0)
		blit((BITMAP*)dataFile[0].dat,mapBmp,(rand()%4)*16,(t-1)*16,x*16,y*16,16,16);

}

void Renderer::BlitMap()
{
	blit(mapBmp,buffer,0,0,0,0,512,512);
}

void Renderer::ClearMapBmp()
{
	clear_bitmap(mapBmp);
}

void Renderer::TestScent(int x,int y,int value)
{
	rect(mapBmp,x*16,y*16,x*16+value/4096,y*16+value/4096,value);
}

void Renderer::DrawSprite(int x,int y,int i)
{
	draw_sprite(buffer,(BITMAP*)dataFile[i].dat,x,y);
}

void Renderer::DrawScore(int x,int y,unsigned long s)
{
	textout_right_ex(buffer,font,"0000000000",x,y,0x445566,0);
	//long has 10 digits max.
	std::ostringstream ss;
	ss<<s;
	textout_right_ex(buffer,font,ss.str().c_str(),x,y,-1,0);
}

void Renderer::Sound(int s)
{
	play_sample((SAMPLE*)dataFile[s].dat,255,127,1000,0);
}

void Renderer::DrawChrono()
{
	std::ostringstream ss;
	int secondes=(chrono/100)%60;
	int minutes=chrono/6000;
	ss<<minutes;
	if (secondes%2==0)
		ss<<":";
	else
		ss<<" ";
	if (secondes<10)
		ss<<"0";
	ss<<secondes;
	Text(ss.str(),150,513,-1,0);
}

void Renderer::LevelCaption(int lvl)
{
	textprintf_ex(buffer,font,384,513,-1,0,"Level %d",lvl);
}
