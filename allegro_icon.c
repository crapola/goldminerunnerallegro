#include <allegro.h>
#if defined ALLEGRO_WITH_XWINDOWS && defined ALLEGRO_USE_CONSTRUCTOR
/* XPM */
static const char* allegico_xpm[] =
{
	/* columns rows colors chars-per-pixel */
	"48 48 13 1 ",
	"  c #000000000000",
	". c #696900000000",
	"X c #696969690000",
	"o c #969600000000",
	"O c #B6B600000000",
	"+ c #D0D000000000",
	"@ c #E0E000000000",
	"# c #FFFF00000000",
	"$ c #969696960000",
	"% c #B6B6B6B60000",
	"& c #D0D0D0D00000",
	"* c #E0E0E0E00000",
	"= c #FFFFFFFF0000",
	/* pixels */
	"#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O",
	"@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o",
	"Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.",
	"#@O                                          #@O",
	"@+o                                          @+o",
	"Oo.                                          Oo.",
	"#@O                                          #@O",
	"@+o                                          @+o",
	"Oo.                                          Oo.",
	"#@O                                          #@O",
	"@+o                                          @+o",
	"Oo.                                          Oo.",
	"#@O         =*%=*%=*%                        #@O",
	"@+o         *&$*&$*&$                        @+o",
	"Oo.         %$X%$X%$X                        Oo.",
	"#@O         =*%=*%=*%                        #@O",
	"@+o         *&$*&$*&$                        @+o",
	"Oo.         %$X%$X%$X                        Oo.",
	"#@O         =*%=*%=*%   =*%=*%               #@O",
	"@+o         *&$*&$*&$   *&$*&$               @+o",
	"Oo.         %$X%$X%$X   %$X%$X               Oo.",
	"#@O                  =*%                     #@O",
	"@+o                  *&$                     @+o",
	"Oo.                  %$X                     Oo.",
	"#@O               =*%   =*%                  #@O",
	"@+o               *&$   *&$                  @+o",
	"Oo.               %$X   %$X                  Oo.",
	"#@O            =*%         =*%      =*%      #@O",
	"@+o            *&$         *&$      *&$      @+o",
	"Oo.            %$X         %$X      %$X      Oo.",
	"#@O                     =*%   =*%=*%         #@O",
	"@+o                     *&$   *&$*&$         @+o",
	"Oo.                     %$X   %$X%$X         Oo.",
	"#@O                     =*%                  #@O",
	"@+o                     *&$                  @+o",
	"Oo.                     %$X                  Oo.",
	"#@O                     =*%                  #@O",
	"@+o                     *&$                  @+o",
	"Oo.                     %$X                  Oo.",
	"#@O                                          #@O",
	"@+o                                          @+o",
	"Oo.                                          Oo.",
	"#@O                                          #@O",
	"@+o                                          @+o",
	"Oo.                                          Oo.",
	"#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O#@O",
	"@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o@+o",
	"Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo.Oo."
};
extern void* allegro_icon;
CONSTRUCTOR_FUNCTION(static void _set_allegro_icon(void));
static void _set_allegro_icon(void)
{
	allegro_icon = allegico_xpm;
}
#endif
