#pragma once
#include "Renderer.h"
#include "Map.h"

class PlayerTestClass
{
	Map* mapPtr;
	short vx,vy;
	char moveTime;
	int image;
	char ticker;
public:
	short x,y;
	bool bAlive;
	char lives;
	unsigned long score;
	PlayerTestClass(Map*);
	~PlayerTestClass(void);
	void Logic();
	void Render();
	bool Move();
	void InitMove(short,short);
	bool Falling();
	void CheckGoldTile();
	void Destroy(int);
	void Reset(int,int);
};
