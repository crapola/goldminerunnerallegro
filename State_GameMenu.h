#pragma once
#include "State.h"


class State_GameMenu : public State
{
	unsigned char keyTimer;
	unsigned int menuSelect;
public:
	unsigned char levelMode;
	State_GameMenu(StateOwner);
	~State_GameMenu(void);
	int Logic();
	void Render();
	void DrawOptions(int);
};
