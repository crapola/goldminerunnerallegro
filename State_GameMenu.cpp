#include "State_GameMenu.h"
#include "State_GameRunning.h"
#include "Game.h"

State_GameMenu::State_GameMenu(StateOwner o):State(o)
{
	menuSelect=0;
	keyTimer=20;
	levelMode=owner.gamePtr->settings.levelMode;
}

State_GameMenu::~State_GameMenu(void)
{
}

int State_GameMenu::Logic()
{
	if (keyTimer>0)
	{
		keyTimer--;
		return 0;
	}

	if (key[KEY_UP]&&menuSelect>0)
	{
		menuSelect--;
		keyTimer=15;
	}
	if (key[KEY_DOWN]&&menuSelect<2)
	{
		menuSelect++;
		keyTimer=15;
	}
	if (key[KEY_J]||key[KEY_K])
	{
		keyTimer=15;
		switch (menuSelect)
		{
		case 0:
			rest(500);
			clear_keybuf();
			State* sss;
			sss=new State_GameRunning(owner);
			owner.gamePtr->ChangeState(sss);
			return 0;
			break;
		case 1:
			levelMode^=1;
			owner.gamePtr->settings.levelMode=levelMode;
			break;
		case 2:
			owner.gamePtr->bRunning=false;
			return 0;
			break;
		}
	}
	return 0;
}

void State_GameMenu::Render()
{
	Renderer::Text("GOLDMINE RUNNER",256,32,-1,2);
	Renderer::Text("v1.1",512,512,0x888888,1);
	owner.gamePtr->DrawHighScores();
	DrawOptions(menuSelect);
}

void State_GameMenu::DrawOptions(int s)
{
	int dif=0x445567;
	Renderer::Text("Start game",168,298,0x445566-dif*(s==0),0);
	Renderer::Text("Mode:",168,312,0x445566-dif*(s==1),0);
	Renderer::Text("Exit",168,328,0x445566-dif*(s==2),0);

	switch (levelMode)
	{
	case 0:
		Renderer::Text("Linear",208,312,0xFF0000-0x880000*(s!=1),0);
		break;
	case 1:
		Renderer::Text("Random",208,312,0xFF0000-0x880000*(s!=1),0);
		break;
	}
}
